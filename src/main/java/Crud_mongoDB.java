import com.mongodb.*;

import java.util.List;
import java.util.Set;

/**
 * Created by yogesh on 7/10/17.
 */
public class Crud_mongoDB extends Connection_mongoDB
{
    String str = "read is done";
//    MongoClient mg;
  //  DBCollection items;





//READ METHOD (read all databases , read collection , read documents)

    public void readDatabases()
    {

        List<String> dbs = mongo.getDatabaseNames();
        System.out.println(dbs);
    }

    //
 public void readCollection()
   {

       DB db = mongo.getDB("project");

       Set<String> collections = db.getCollectionNames();
       System.out.println(collections);


       System.out.println(""+ str);
    }

    //
   public void readDocuments()
   {
        db = mongo.getDB( "project" );
       System.out.println("Connect to database successfully");

       DBCollection coll = db.getCollection("newcollection");
      // System.out.println("Collection patient selected successfully");

       DBCursor cursor = coll.find();
       int i = 1;
       while (cursor.hasNext()) {
           System.out.println("Inserted Document: "+i);
           System.out.println(cursor.next());
           i++;
       }
   }

   //CREATE METHOD (create databases , create collection , create documents)

    public void createCollection()
    {
         db = mongo.getDB( "NewDB" );
        System.out.println("Connect to database successfully");

        DBCollection coll = db.createCollection("newcollection" , null);
        System.out.println("Collection created successfully");

    }
    //
    public void createDocument()
    {
        db = mongo.getDB( "project" );
        System.out.println("Connect to database successfully");
      //  DBCollection coll = db.createCollection("newcollection" , null);
        //System.out.println("Collection created successfully");
         DBCollection coll = db.getCollection("newcollection");
        System.out.println("Collection selected successfully");
        BasicDBObject doc = new BasicDBObject("title", "MongoDB").
                append("description", "database").
                append("likes", 100).
                append("url", "http://www.google.com/").
                append("by", "google");

        coll.insert(doc);
        System.out.println("Document inserted successfully");

    }

    //DELETE METHOD (delete document)
    public void deleteDocument()
    {
         db = mongo.getDB( "project" );
        System.out.println("Connect to database successfully");
        DBCollection coll = db.getCollection("newcollection");
        System.out.println("Collection mycol selected successfully");

        DBObject myDoc = coll.findOne();
        coll.remove(myDoc);
        DBCursor cursor = coll.find();
        int i = 1;

        while (cursor.hasNext()) {
            System.out.println("Inserted Document: "+i);
            System.out.println(cursor.next());
            i++;
        }

        System.out.println("Document deleted successfully");


    }

    // UPDATE METHOD (update document)

    public void UpdateDocument()
    {

        BasicDBObject updatedoc = new BasicDBObject();
        //new value
        updatedoc.append("$set" , new BasicDBObject()
          .append("likes" ,500));

        //old document
        BasicDBObject olddoc = new BasicDBObject().append("title" , "MongoDB");
        DBCollection coll = db.getCollection("newcollection");
        coll.update(olddoc,updatedoc,false,false);
        System.out.println("document update");
        Cursor cursor = coll.find();



    }





}
